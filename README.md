# foxyd0ts ***WIP***

# ******* Installation *******

# **** What is used ***

wallpaper = Synthwave Neon 80s Background Rafal De Jongh via rafaeldejongh.com

shell       =   zsh

term        =   kitty

desktop     =   awesomewm

compositor  =   picom

fileManager =   Thunar

WIP theme   =   dracula

WIP icon    =   dracula

*** Reqierments ***

OS : Arch Linux

ansible git python-pip

*** Setup ***

Manual installation - Just clone the ansible-aur repository into your user custom-module directory:

> git clone https://github.com/kewlfft/ansible-aur.git ~/.ansible/plugins/modules/aur

> ansible-playbook ./laptop-workstation.yml -i inventory/hosts
