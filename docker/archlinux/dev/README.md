# ******* Dockerfile *******

*** Reqierments ***

Docker Xephyr

*** Setup ***

> sudo docker build -t archlinux/foxyd0ts-dev docker/archlinux/dev/Dockerfile .

> Xephyr :1 -ac -br -screen 1024x768 -resizeable -reset

> sudo docker run -e DISPLAY=:1 -v /tmp/.X11-unix:/tmp/.X11-unix archlinux/foxyd0ts-dev awesome
