#!/usr/bin/env bash

#function from https://wiki.archlinux.org/title/Awesome#Autostart

function run {
  if ! pgrep -f $1 ;
  then
    $@&
  fi
}

# Network Manager Applet
run nm-applet

# Disable Bell
#run "xset -b"

#run "~/.screenlayout/layout.sh"

# Bluetooth
#run "blueman-applet"

# Battey gui
#run "mate-power-manager"

# Compositor
#run "picom --experimental-backends --config " .. gears.filesystem.get_configuration_dir() .. "configs/picom.con"
run picom -b --config ~/.config/picom/picom.conf

# Transparency
#run "xcompmgr"

# Remove mouse when idle
run unclutter

run xrdb -load $HOME/.Xresources

# run "discord"

# run "telegram-desktop"
